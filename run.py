from flask import Flask, render_template, request
from pymongo import MongoClient
import json
import os

app = Flask(__name__)

def get_mongo_db():
    client = MongoClient(os.environ['MONGO_CONTAINER_PORT_27017_TCP_ADDR'], 27017)
    db = client.local
    return db

def load_data():
    json_data = None
    with open('./sensor_data.json', 'r') as f:
        json_data = json.load(f)
    db = get_mongo_db()
    db.sensor_data.drop()
    insert_ids = db.sensor_data.insert_many(json_data)
    print insert_ids
    return True

@app.route('/', methods=['GET'])
def home_api():
    db = get_mongo_db()
    collection = db.sensor_data
    match_query = {'$match': {}}
    ip_address = request.args.get('ip_address', None)
    snort_rule_id = request.args.get('snort_rule_id', None)
    ip_address_exclude = request.args.get('ip_address_exclude', False)
    snort_rule_id_exclude = request.args.get('snort_rule_id_exclude', False)

    if ip_address:
        match_query['$match']['communication_pairs'] = {'$elemMatch': {'$or':[{'source_ip':ip_address}, {'dest_ip':ip_address}]}}
        if ip_address_exclude:
            match_query['$match']['communication_pairs'] = {'$not': {'$elemMatch': {'$or':[{'source_ip':ip_address}, {'dest_ip':ip_address}]}}}

    if snort_rule_id:
        match_query['$match']['sid'] = {'$regex': str("\:" + snort_rule_id + "\:"), '$options':'i'}
        if snort_rule_id_exclude:
            match_query['$match']['sid'] = {'$not': match_query['$match']['sid']}

    print match_query
    aggregate_common = [
        {'$unwind': '$communication_pairs'},
        {'$sort': {'communication_pairs.alert_count_sum': -1}},
        {'$group': {
            '_id': '$_id',
            'alert_count_sum': {'$first': '$alert_count_sum'},
            'message': {'$first': '$message'},
            'host': {'$first': '$host'},
            'category': {'$first': '$category'},
            'sid': {'$first': '$sid'},
            'priority': {'$first': '$priority'},
            'communication_pairs': {'$push': '$communication_pairs'}
        }},
        {'$sort': {'alert_count_sum': -1}},
        {'$project': {
            'alert_count_sum': 1,
            'message': 1,
            'host': 1,
            'category': 1,
            'sid': 1,
            'priority': 1,
            'communication_pairs': {'$slice': ['$communication_pairs', 10]}
        }}
    ]
    aggregate_common.insert(0, match_query)
    data_rows = collection.aggregate(aggregate_common)
    return render_template('index.html', table_data=data_rows)

if __name__ == '__main__':
    load_data()
    app.run(debug=True, host='0.0.0.0', port=8001)
