# N-Dimenions Task

##### Stack:
-- Mongodb

-- Flask (Python)

##### Instructions to run app:

1. Clone this repository and enter the directory



2. Start a default mongodb container
    
    ```
    docker run -p 27017:27017 --name mongo-container -d mongo:latest
    ```
    
    **Note:** the mongo container name is important as I will be linking it to my flask container
    


3. Build and run n-dimensions flask container
    
    ```
    docker build -t n-dimensions-task:latest .
    ```
    
    ```
    docker run -p 8001:8001 --link mongo-container --name flask-container n-dimensions-task:latest
    ```


4. Go to http://localhost:8001 to see the application in action

##### About the approach
Since our data is in the json format the appropriate choice here was to use some document based database (mongodb, couchdb, etc.) which makes it easier to store and query json like structures. Similar result can be achieved using a SQL database like mysql but that would require us to pre-process the incoming data before storing it in different tables (and make relations), and would add an overhead of queries to reconstruct the document back for display.

I used a simple flask app with 1 route to query mongodb and get the desired results and display it using jquery datatables.

For the sake of convenience, I have written a code to read the sample `sensor_data.json` and load it in a mongodb collection before starting the flask app.