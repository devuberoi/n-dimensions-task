import json
import os
from pymongo import MongoClient

def get_data():
    json_data = None
    with open('./sensor_data.json', 'r') as f:
        json_data = json.load(f)
    return json_data

def feed_mongo(json_data):
    client = MongoClient(os.environ['MONGO_CONTAINER_PORT_27017_TCP_ADDR'], 27017)
    db = client.local
    db.sensor_data.drop()
    insert_ids = db.sensor_data.insert_many(json_data)
    return insert_ids

if __name__ == '__main__':
    json_data = get_data()
    if json_data:
        insert_ids = feed_mongo(json_data)
        print insert_ids
